package com.tz.abstractfactory;
/**
 * 抽象工厂模式
 * @author Administrator
 *
 */
public class Client {

	public static void main(String[] args) {
		AbstractFactory factory = new ConcreteFactory2();
		System.out.println(factory.createProductA());
		System.out.println(factory.createProductB());
	}

}
