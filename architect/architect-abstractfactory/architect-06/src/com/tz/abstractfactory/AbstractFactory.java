package com.tz.abstractfactory;

/**
 * 抽象工厂
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public interface AbstractFactory {

	/**
	 * 生成A类型的产品
	 * @return
	 */
	AbstractProductA createProductA();
	
	/**
	 * 生产B类型的产品
	 * @return
	 */
	AbstractProductB createProductB();
	
}
