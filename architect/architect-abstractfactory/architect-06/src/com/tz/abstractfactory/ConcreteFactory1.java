package com.tz.abstractfactory;

/**
 * 抽象工厂的具体实现一，创建具体的产品簇
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class ConcreteFactory1 implements AbstractFactory{

	@Override
	public AbstractProductA createProductA() {
		return new ProductA1();
	}

	@Override
	public AbstractProductB createProductB() {
		return new ProductB1();
	}

}
