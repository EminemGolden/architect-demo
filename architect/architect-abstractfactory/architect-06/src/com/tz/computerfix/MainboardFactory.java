package com.tz.computerfix;

/**
 * 主板工厂
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class MainboardFactory {

	/**
	 * 生产主板
	 * @param type 1 技嘉1156，2微星939
	 * @return
	 */
	public static MainboardApi createMainboardApi(int type) {
		MainboardApi mainboard = null;
		switch (type) {
		case 1:
			mainboard = new GAMainboard(1156);
			break;

		case 2:
			mainboard = new MSIMainboard(939);
			break;

		default:
			break;
		}
		return mainboard;
	}

}
