package com.tz.computerfix;

/**
 * 微星主板
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class MSIMainboard implements MainboardApi {

	private int cpuHoles;

	public MSIMainboard(int cpuHoles) {
		super();
		this.cpuHoles = cpuHoles;
	}

	@Override
	public void installCPU() {
		System.out.println("微星的主板.cpuHoles:"+cpuHoles);
	}

}
