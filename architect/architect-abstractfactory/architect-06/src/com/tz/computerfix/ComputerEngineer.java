package com.tz.computerfix;

/**
 * 装机工程师
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class ComputerEngineer {

	
	private CPUApi cpu;
	private MainboardApi mainboard;


	/**
	 * 根据客户提供的配件类型组装电脑
	 * @param cpuType
	 * @param mainboardType
	 */
	public void makeComputer(int cpuType,int mainboardType){
		//1.准备硬件
		prepareHardwares(cpuType,mainboardType);
		//2.组装
		//3.测试
		//4.交付
	}

	
	/**
	 * 准备硬件
	 * @param cpuType
	 * @param mainboardType
	 */
	private void prepareHardwares(int cpuType, int mainboardType) {
		cpu = CPUFactory.createCPUApi(cpuType);
		mainboard = MainboardFactory.createMainboardApi(mainboardType);
		
		//测试配件
		cpu.calculate();
		mainboard.installCPU();
	}
	
	
}
