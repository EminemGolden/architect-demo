package com.tz.computerfix;

/**
 * AMD CPU实现
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class AMDCpu implements CPUApi {

	private int pins;

	public AMDCpu(int pins) {
		super();
		this.pins = pins;
	}

	@Override
	public void calculate() {
		System.out.println("AMD CPU计算.pins:"+pins);
	}

}
