package com.tz.abstractfactory2;

/**
 * 英特尔CPU的实现
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class IntelCPU implements CPUApi {

	/**
	 * 针脚数目
	 */
	private int pins;

	public IntelCPU(int pins) {
		super();
		this.pins = pins;
	}

	@Override
	public void calculate() {
		System.out.println("因特尔CPU计算.pins:"+pins);
	}

}
