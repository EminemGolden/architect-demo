package com.tz.abstractfactory2;


public class Client {

	public static void main(String[] args) {
		ComputerEngineer engineer = new ComputerEngineer();
		//客户选择方案
		AbstractFactory schema = new Schema2();
		//装机工程师负责装机
		engineer.makeComputer(schema);
	}

}
