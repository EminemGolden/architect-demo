package com.tz.abstractfactory2;

/**
 * 方案一
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class Schema2 implements AbstractFactory{

	@Override
	public CPUApi createProductA() {
		return new AMDCpu(939);
	}

	@Override
	public MainboardApi createProductB() {
		return new MSIMainboard(939);
	}

}
