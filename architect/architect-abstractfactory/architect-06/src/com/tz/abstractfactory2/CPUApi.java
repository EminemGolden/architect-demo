package com.tz.abstractfactory2;

/**
 * CPU接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public interface CPUApi {

	/**
	 * 计算
	 */
	void calculate();
	
}
