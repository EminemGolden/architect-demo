package com.tz.abstractfactory2;


/**
 * 方案一
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class Schema1 implements AbstractFactory{

	@Override
	public CPUApi createProductA() {
		return new IntelCPU(1156);
	}

	@Override
	public MainboardApi createProductB() {
		return new GAMainboard(1156);
	}

}
