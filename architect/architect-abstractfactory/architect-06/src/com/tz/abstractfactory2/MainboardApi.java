package com.tz.abstractfactory2;

/**
 * 主板接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public interface MainboardApi {

	/**
	 * 安装CPU
	 */
	void installCPU();
	
}
