package com.tz.abstractfactory2;

public interface AbstractFactory {

	/**
	 * 生成A类型的产品
	 * @return
	 */
	CPUApi createProductA();
	
	/**
	 * 生产B类型的产品
	 * @return
	 */
	MainboardApi createProductB();
	
}
