package com.tz.abstractfactory3;

/**
 * 方案二
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class Schema2 implements AbstractFactory{

	@Override
	public Component createProduct(int type) {
		Component obj = null;
		//type为1代表CPU，type为2代表主板
		switch (type) {
		case 1:
			obj = new AMDCpu(939);
			break;
		case 2:
			obj = new MSIMainboard(939);
			break;	
		case 3:
			obj = new KingstonMemory();
			break;		
		default:
			break;
		}
		return obj;
	}

}
