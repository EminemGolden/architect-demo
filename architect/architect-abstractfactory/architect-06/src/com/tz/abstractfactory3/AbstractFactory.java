package com.tz.abstractfactory3;

public interface AbstractFactory {

	/**
	 * 生成电脑需要的配件
	 * @param type
	 * @return
	 */
	Component createProduct(int type);
	
}
