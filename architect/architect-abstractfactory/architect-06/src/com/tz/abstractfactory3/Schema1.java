package com.tz.abstractfactory3;

/**
 * 方案一
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class Schema1 implements AbstractFactory{

	@Override
	public Component createProduct(int type) {
		Component obj = null;
		//type为1代表CPU，type为2代表主板
		switch (type) {
		case 1:
			obj = new IntelCPU(1156);
			break;
		case 2:
			obj = new GAMainboard(1156);
			break;	
		case 3:
			obj = new SamsungMemory();
			break;		
		default:
			break;
		}
		return obj;
	}

}
