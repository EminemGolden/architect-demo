package com.tz.abstractfactory3;

/**
 * 内存接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public interface MemoryApi extends Component{

	/**
	 * 存储
	 */
	void memorySave();
	
}
