package com.tz.abstractfactory3;

/**
 * 三星内存 
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class SamsungMemory implements MemoryApi{

	
	@Override
	public void memorySave() {
		System.out.println("三星内存");
	}	

}
