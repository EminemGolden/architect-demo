package com.tz.abstractfactory3;

/**
 * 技嘉主板
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月7日
 * @version 1.0
 */
public class GAMainboard implements MainboardApi {

	/**
	 * CPU 插槽的孔数
	 */
	private int cpuHoles;

	public GAMainboard(int cpuHoles) {
		super();
		this.cpuHoles = cpuHoles;
	}

	@Override
	public void installCPU() {
		System.out.println("技嘉的主板，cpuHoles:"+cpuHoles);
	}

}
