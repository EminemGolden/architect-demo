package com.jason.interator;

/**
 * 容器接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public interface Aggregate<T> {

	/**
	 * 添加元素
	 * @param obj
	 */
	void add(T obj);
	
	/**
	 * 删除元素
	 * @param obj
	 */
	void remove(T obj);
	
	/**
	 * 获取容器的迭代器
	 * @return 迭代器对象
	 */
	Iterator<T> iterator();
}
