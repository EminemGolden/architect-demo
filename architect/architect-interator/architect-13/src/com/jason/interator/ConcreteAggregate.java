package com.jason.interator;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体的容器
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class ConcreteAggregate<T> implements Aggregate<T>{

	private List<T> list = new ArrayList<T>();
	
	@Override
	public void add(T obj) {
		list.add(obj);
	}

	@Override
	public void remove(T obj) {
		list.remove(obj);
	}

	@Override
	public Iterator<T> iterator() {
		return new ConcreteIterator<>(list);
	}

	
	
}
