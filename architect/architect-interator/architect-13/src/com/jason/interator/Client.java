package com.jason.interator;

public class Client {

	public static void main(String[] args) {
		//容器
		Aggregate<String> collection = new ConcreteAggregate<String>();
		collection.add("湖南卫视");
		collection.add("浙江卫视");
		collection.add("江苏卫视");
		collection.add("东方卫视");
		
		//遍历
		Iterator<String> iterator = collection.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}

}
