package com.jason.interator;

import java.util.List;

/**
 * 具体的迭代器实现
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class ConcreteIterator<T> implements Iterator<T> {
	//集合
	private List<T> list;
	//位置
	private int position;

	public ConcreteIterator(List<T> list) {
		super();
		this.list = list;
	}

	@Override
	public boolean hasNext() {
		return position != list.size();
	}

	@Override
	public T next() {
		if(hasNext()){
			return list.get(position++);
		}
		return null;
	}


}
