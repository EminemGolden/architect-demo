package com.jason.interator;

/**
 * 迭代器接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public interface Iterator<T> {

	/**
	 * 是否还有下一个元素
	 * @return
	 */
	boolean hasNext();
	
	/**
	 * 返回当前位置元素，并且将位置移至下一位
	 * @return
	 */
	T next();
	
}
