package com.jason.interator2;

/**
 * 桔子科技
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class JuziCompany {

	private Employee[] array = new Employee[3];
	
	public JuziCompany(){
		array[0] = new Employee("柳青", 40, "女", "总裁");
		array[1] = new Employee("李开复", 40, "男", "程序员");
		array[2] = new Employee("张亚勤", 35, "男", "程序员");
	}
	
	public Employee[] getEmployees(){
		return array;
	}
	
}
