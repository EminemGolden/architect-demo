package com.jason.interator2;

import java.util.List;

public class Client {

	public static void main(String[] args) {
		AliCompany ac = new AliCompany();
		//list
		List<Employee> c1 = ac.getEmployees();
		for (int i = 0; i < c1.size(); i++) {
			System.out.println(c1.get(i));
		}
		
		System.out.println("--------------");
		//数组
		JuziCompany jc = new JuziCompany();
		Employee[] c2 = jc.getEmployees();
		for (int i = 0; i < c2.length; i++) {
			System.out.println(c2[i]);
		}
	}
	
	//无法提供一个通用的方法，用来实现所有的员工，必须暴露细节
	public void show(){
		
	}

}
