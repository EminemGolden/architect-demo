package com.jason.interator2;

/**
 * 员工
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class Employee {

	// 姓名
	private String name;
	// 年龄
	private int age;
	// 性别
	private String sex;
	// 职位
	private String position;

	public Employee(String name, int age, String sex, String position) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", sex=" + sex + ", position=" + position + "]";
	}

}
