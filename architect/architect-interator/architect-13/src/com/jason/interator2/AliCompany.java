package com.jason.interator2;

import java.util.ArrayList;
import java.util.List;

/**
 * 阿里巴巴公司
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class AliCompany {

	private List<Employee> list = new ArrayList<Employee>();
	
	//添加元素
	public AliCompany(){
		list.add(new Employee("杀死凯", 20, "女", "程序员鼓励师"));
		list.add(new Employee("Maybe", 20, "女", "程序员按摩师"));
		list.add(new Employee("高山流水", 18, "男", "霸道总裁"));
		list.add(new Employee("LEO", 16, "女", "秘书"));
	}
	
	public List<Employee> getEmployees(){
		return list;
	}
	
}
