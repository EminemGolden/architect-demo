package com.jason.interator3;

/**
 * 公司接口（Aggregate容器接口）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public interface Company {

	
	/**
	 * 返回一个迭代器
	 * @return
	 */
	Iterator iterator();
	
}
