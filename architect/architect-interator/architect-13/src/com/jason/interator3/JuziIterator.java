package com.jason.interator3;

/**
 * 桔子科技员工迭代器
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class JuziIterator implements Iterator {

	private Employee[] array;
	private int position;

	public JuziIterator(Employee[] array) {
		super();
		this.array = array;
	}

	@Override
	public boolean hasNext() {
		return position != array.length;
	}

	@Override
	public Employee next() {
		if(hasNext()){
			return array[position++];
		}
		return null;
	}

}
