package com.jason.interator3;

public class Client {

	public static void main(String[] args) {
		//容器一
		AliCompay ac = new AliCompay();
		
		//容器二
		JuziCompany jc = new JuziCompany();
		
		//遍历
		show(ac);
		show(jc);
	}
	
	public static void show(Company company){
		Iterator iterator = company.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}

}
