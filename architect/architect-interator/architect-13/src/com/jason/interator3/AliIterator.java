package com.jason.interator3;

import java.util.List;

/**
 * 迭代遍历阿里的员工
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class AliIterator implements Iterator {

	private List<Employee> list;
	private int position;

	public AliIterator(List<Employee> list) {
		super();
		this.list = list;
	}

	@Override
	public boolean hasNext() {
		return position != list.size();
	}

	@Override
	public Employee next() {
		if(hasNext()){
			return list.get(position++);
		}
		return null;
	}

}
