package com.jason.interator3;

import java.util.ArrayList;
import java.util.List;



/**
 * 阿里公司（ConcreteAggregate）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月31日
 * @version 1.0
 */
public class AliCompay implements Company {
	
	private List<Employee> list = new ArrayList<Employee>();
	
	public AliCompay() {
		list.add(new Employee("杀死凯", 20, "女", "程序员鼓励师"));
		list.add(new Employee("Maybe", 20, "女", "程序员按摩师"));
		list.add(new Employee("高山流水", 18, "男", "霸道总裁"));
		list.add(new Employee("LEO", 16, "女", "秘书"));
	}
	
	@Override
	public Iterator iterator() {
		return new AliIterator(list);
	}


}
