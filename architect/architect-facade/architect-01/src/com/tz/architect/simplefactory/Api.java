package com.tz.architect.simplefactory;

public interface Api {

	void operation();
	
}
