package com.tz.architect.simplefactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AdvancedFactory {

	private static String impl;
	static{
		try {
			//加载配置文件
			Properties props = new Properties();
			InputStream is = AdvancedFactory.class.getClassLoader().getResourceAsStream("config.properties");
			props.load(is);
			impl = props.getProperty("impl");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 产生Api的实例，如果需要制定实例，请配置config.properties文件
	 * @return
	 */
	public static Api createApi(){
		try {
			Class implCls = Class.forName(impl);
			return (Api)implCls.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
