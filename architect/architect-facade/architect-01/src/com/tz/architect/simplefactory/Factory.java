package com.tz.architect.simplefactory;

public class Factory {

	private Factory() {
	}
	
	public static Api createApi(int condition){
		switch (condition) {
		case 1:
			return new ImplA();
		case 2:
			return new ImplB();
		default:
			break;
		}
		return null;
	}
	
}
