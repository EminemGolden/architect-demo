package com.tz.architect.simplefactory;

public class Client {

	public static void main(String[] args) {
		Api api = Factory.createApi(2);
		api.operation();
	}

}
