package com.tz.architect.facade;

public class Facade {

	public void generate(){
		ModelModuleGen mmg = new ModelModuleGen();
		mmg.generate();
		
		DaoModuleGen dmg = new DaoModuleGen();
		dmg.generate();
		
		BizModuleGen bmg = new BizModuleGen();
		bmg.generate();
	}
	
}
