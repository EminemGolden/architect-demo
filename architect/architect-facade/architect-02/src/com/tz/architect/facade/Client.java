package com.tz.architect.facade;

public class Client {

	public static void main(String[] args) {
		Facade f = new Facade();
		f.generate();
	}

}
