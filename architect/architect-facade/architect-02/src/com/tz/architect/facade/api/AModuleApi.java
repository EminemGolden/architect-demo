package com.tz.architect.facade.api;

public interface AModuleApi {

	void operate();
	
}
