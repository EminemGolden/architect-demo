package com.tz.architect.facade.api;

public interface BModuleApi {

	void operate();
	
}
