package com.tz.architect.facade.api;

public class Facade {

	private Facade() {
	}
	
	public static void operate(){
		AModuleApi am = new AModuleImpl();
		am.operate();
		
		BModuleApi bm = new BModuleImpl();
		bm.operate();
	}
	
}
