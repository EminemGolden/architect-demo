package com.jason.command2;

/**
 * 接收者（干活的）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class TetrisMachine {

	/**
	 * 真正处理“向左”操作的逻辑代码
	 */
	public void toLeft(){
		System.out.println("向左");
	}
	
	/**
	 * 向右
	 */
	public void toRight(){
		System.out.println("向右");
	}
	
	/**
	 * 快速向下
	 */
	public void fastToBottom(){
		System.out.println("快速落下");
	}
	
	/**
	 * 变形
	 */
	public void transform(){
		System.out.println("改变形状");
	}
	
}
