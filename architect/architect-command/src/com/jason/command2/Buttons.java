package com.jason.command2;

/**
 * 请求者
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class Buttons {

	// 持有命令对象
	private LeftCommand leftCommand;
	private RightCommand rightCommand;
	private FallCommand fallCommand;
	private TransformCommand transformCommand;

	//不同的方法，执行不同的命令
	public void toLeft(){
		leftCommand.execute();
	}
	
	public void toRight(){
		rightCommand.execute();
	}
	
	public void fall(){
		fallCommand.execute();
	}
	
	public void transform(){
		transformCommand.execute();
	}
	
	public void setLeftCommand(LeftCommand leftCommand) {
		this.leftCommand = leftCommand;
	}

	public void setRightCommand(RightCommand rightCommand) {
		this.rightCommand = rightCommand;
	}

	public void setFallCommand(FallCommand fallCommand) {
		this.fallCommand = fallCommand;
	}

	public void setTransformCommand(TransformCommand transformCommand) {
		this.transformCommand = transformCommand;
	}

}
