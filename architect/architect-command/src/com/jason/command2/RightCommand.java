package com.jason.command2;

/**
 * 向右命令
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class RightCommand implements Command {

	private TetrisMachine receiver;

	public RightCommand(TetrisMachine receiver) {
		super();
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.toRight();
	}

}
