package com.jason.command2;

/**
 * 向左的命令
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class LeftCommand implements Command {

	private TetrisMachine receiver;

	public LeftCommand(TetrisMachine receiver) {
		super();
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.toLeft();
	}

}
