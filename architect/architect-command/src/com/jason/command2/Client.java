package com.jason.command2;

public class Client {

	public static void main(String[] args) {
		//接收者
		TetrisMachine receiver = new TetrisMachine();
		
		//命令
		LeftCommand leftCommand = new LeftCommand(receiver);
		RightCommand rightCommand = new RightCommand(receiver);
		FallCommand fallCommand = new FallCommand(receiver);
		TransformCommand transformCommand = new TransformCommand(receiver);
		
		//请求
		Buttons button = new Buttons();
		button.setLeftCommand(leftCommand);
		button.setRightCommand(rightCommand);
		button.setFallCommand(fallCommand);
		button.setTransformCommand(transformCommand);
		
		//执行请求
		button.toLeft();
		button.toRight();
		button.fall();
		button.transform();
		
	}

}
