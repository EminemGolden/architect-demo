package com.jason.command2;

public class Client2 {

	public static void main(String[] args) {
		TetrisMachine receiver = new TetrisMachine();
		receiver.toLeft();
		receiver.toRight();
		receiver.fastToBottom();
		receiver.transform();

	}

}
