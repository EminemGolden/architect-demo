package com.jason.command;

/**
 * 接收者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class Receiver {

	/**
	 * 行动方法，执行具体的逻辑
	 */
	public void action(){
		System.out.println("执行具体的逻辑");
	}
	
}
