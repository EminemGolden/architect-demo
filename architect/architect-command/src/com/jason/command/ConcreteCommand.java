package com.jason.command;

/**
 * 具体的命令
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class ConcreteCommand implements Command {

	// 持有接收者的引用
	private Receiver receiver;

	public ConcreteCommand(Receiver receiver) {
		super();
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		//执行命令，真正还是调用的Receiver的行动方法
		receiver.action();
	}

}
