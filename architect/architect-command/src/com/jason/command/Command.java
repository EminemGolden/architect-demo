package com.jason.command;

/**
 * 命令接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public interface Command {

	/**
	 * 执行命令
	 */
	void execute();
	
}
