package com.jason.command;

public class Client {

	public static void main(String[] args) {
		//接收者
		Receiver receiver = new Receiver();
		
		//命令对象
		Command command = new ConcreteCommand(receiver);
		
		//请求者
		Invoker invoker = new Invoker(command);
		
		//执行请求
		invoker.action();
	}

}
