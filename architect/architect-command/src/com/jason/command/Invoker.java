package com.jason.command;

/**
 * 请求者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class Invoker {

	//持有命令对象
	private Command command;
	
	public Invoker(Command command) {
		super();
		this.command = command;
	}


	/**
	 * 执行请求
	 */
	public void action(){
		command.execute();
	}
	
}
