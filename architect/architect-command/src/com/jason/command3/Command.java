package com.jason.command3;

import java.io.Serializable;

/**
 * 命令接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public interface Command extends Serializable{

	/**
	 * 执行命令
	 */
	void execute();
	
}
