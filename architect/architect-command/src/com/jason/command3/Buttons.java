package com.jason.command3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 请求者 有日志记录，可以恢复
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月28日
 * @version 1.0
 */
public class Buttons {

	//持有了接收者
	private TetrisMachine receiver;
	
	//命令列表
	private List<Command> commands = new ArrayList<Command>();

	public Buttons(TetrisMachine receiver) {
		super();
		this.receiver = receiver;
	}

	// 不同的方法，执行不同的命令
	public void toLeft() {
		LeftCommand leftCommand = new LeftCommand(receiver);
		leftCommand.execute();
		commands.add(leftCommand);
	}

	public void toRight() {
		RightCommand rightCommand = new RightCommand(receiver);
		rightCommand.execute();
		commands.add(rightCommand);
	}

	public void fall() {
		FallCommand fallCommand = new FallCommand(receiver);
		fallCommand.execute();
		commands.add(fallCommand);
	}

	public void transform() {
		TransformCommand transformCommand = new TransformCommand(receiver);
		transformCommand.execute();
		commands.add(transformCommand);
	}

	/**
	 * 退出时，保存进度
	 */
	public void exitOnSave(){
		File file = new File("C://progress.txt");
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			os.writeObject(commands);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 从上一次记录开始玩起
	 */
	public void playOldProgress(){
		File file = new File("C://progress.txt");
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
			commands = (List<Command>) is.readObject();
			if(commands != null){
				for (Command command : commands) {
					command.execute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
