package com.jason.command3;

public class Client {

	public static void main(String[] args) {
		//接收者
		TetrisMachine receiver = new TetrisMachine();
		
		
		//请求
		Buttons button = new Buttons(receiver);
		
		button.toLeft();
		button.toRight();
		button.toRight();
		
		//退出，保存
		button.exitOnSave();
		System.out.println("保存退出");
		
		System.out.println("-----------------");
		System.out.println("重新进入游戏");
		//从上一次记录开始玩起
		button.playOldProgress();
		
		button.transform();
		button.fall();
		
	}

}
