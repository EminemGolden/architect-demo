package com.tz.architect.singleton.myenum;

/**
 * James Gosling推荐
 * 《Effective Java》《Java高效编程》
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public enum Singleton {

	instance;
	
	
	public void operate(){
		System.out.println("处理...");
	}
	
}

