package com.tz.architect.singleton.myenum;

public enum MyEnum{
	
	//monday, tuesday, wensday;
	
	RED(1,"红色"), BLUE(2,"蓝色");
	
	private int index;
	private String name;
	
	MyEnum(int index, String name){
		this.index = index;
		this.name = name;
	}
	
	public int getIndex() {
		return index;
	}
	
	public String getName() {
		return name;
	}	
	
}

class Test{
	public static void main(String[] args) {
		MyEnum.BLUE.getName();
		MyEnum.RED.getIndex();
		
		Singleton.instance.operate();
	}
}