package com.tz.architect.singleton.innerclass;

/**
 * 静态内部类
 * 既能够做到延迟加载，又能够实现线程安全
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class Singleton {

	private Singleton(){
		
	}
	
	private static class SingletonHolder{
		//静态内部类的静态属性实例化，由JVM保证线程的安全
		private static Singleton instance =  new Singleton();
	}
	
	//只有调用了getInstance方法之后，才会实例化SingletonHolder.instance属性
	public static Singleton getInstance(){
		return SingletonHolder.instance;
	}
}
