package com.tz.architect.singleton.objectpool;

import java.util.HashMap;
import java.util.Map;

public class Connection {

	private static Map<Integer, Connection> map = new HashMap<Integer,Connection>();
	
	private static int key = 1;
	
	//缓存迟的最大个数
	private static int max = 5;
	
	private Connection(){
		
	}
	
	public static Connection getConnection(){
		Connection instance = map.get(key);
		if(instance == null){
			instance = new Connection();
			map.put(key, instance);
		}
		key++;
		if(key > 5){
			key = 1;
		}
		return instance;
	}
}


