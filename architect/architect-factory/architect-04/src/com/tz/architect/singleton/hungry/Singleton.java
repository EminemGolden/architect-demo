package com.tz.architect.singleton.hungry;

/**
 * 饿汉式（无法向单例对象的构造方法传参）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class Singleton {

	private static Singleton instance = new Singleton();
	
	private Singleton(){
		
	}
	
	public static Singleton getInstance(){
		return instance;
	}
	
}
