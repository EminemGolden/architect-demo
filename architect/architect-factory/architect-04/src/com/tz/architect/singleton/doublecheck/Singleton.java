package com.tz.architect.singleton.doublecheck;

/**
 * 双重检查
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class Singleton {

	//线程并发时，当变量被修改之后，线程们能即时访问到最新的数据
	//会屏蔽掉虚拟机的一些代码优化
	private volatile static Singleton instance;
	
	private Singleton(){
		
	}
	
	public static Singleton getInstance(){
		if(instance == null){
			synchronized (Singleton.class) {
				if(instance == null){
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
	
}
