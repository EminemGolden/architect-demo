package com.tz.architect.singleton.lazy;

/**
 * 懒汉式
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class Singleton {

	private static Singleton instance;
	
	private Singleton(){
		
	}
	
	public static synchronized Singleton getInstance(){
		if(instance == null){
			instance = new Singleton();
		}
		return instance;
	}
	
}
