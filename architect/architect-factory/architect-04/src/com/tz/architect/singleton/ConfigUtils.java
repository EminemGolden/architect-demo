package com.tz.architect.singleton;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtils {

	
	private static ConfigUtils instance = new ConfigUtils();
	
	private String user;
	private String password;
	
	private ConfigUtils() {
		load();
	}
	//读取配置文件
	private void load(){
		try {
			Properties props = new Properties();
			InputStream is = ConfigUtils.class.getClassLoader().getResourceAsStream("config.properties");
			props.load(is);
			this.user = props.getProperty("user");
			this.password = props.getProperty("password");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static ConfigUtils getInstance(){
		return instance;
	}
	
	
	public String getUser() {
		return user;
	}
	
	public String getPassword() {
		return password;
	}
	
}
