package com.tz.architect.export.lib2;

/**
 * 产品
 * 导出数据文件（数据文件可能有，数据库文件、文本文件、XML文件、Excel文件等等）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public interface ExportFileApi {

	/**
	 * 导出内容为文件
	 * @param data 需要导出的数据
	 * @return 是否导出成功
	 */
	public boolean export(String data);
}
