package com.tz.architect.export.lib;

/**
 * 导出数据为文本文件的操作
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class ExportTextFileOperator extends ExportOperate{

	@Override
	protected ExportFileApi factoryMethod() {
		return new ExportTextFile();
	}

}
