package com.tz.architect.export.lib2;

/**
 * 创建者
 * 执行导出操作
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public abstract class ExportOperate {

	/**
	 * 工厂方法，用于创建导出数据的文件对象
	 * @param type
	 * @return
	 */
	protected abstract ExportFileApi factoryMethod(int type);
		
	
	public boolean export(String data,int type){
		//导出特定文件
		ExportFileApi api = factoryMethod(type);
		return api.export(data);
	}
	
}
