package com.tz.architect.export.lib2;

/**
 * 导出数据为数据库文件
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class ExportOracleDBFile implements ExportFileApi{

	@Override
	public boolean export(String data) {
		System.out.println("数据："+data);
		System.out.println("导出生成了oracle数据库文件..");
		return false;
	}

}
