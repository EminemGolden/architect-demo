package com.tz.architect.export.lib2;

/**
 * 导出数据为数据库文件的操作
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public class ExportDBFileOperator extends ExportOperate{

	@Override
	protected ExportFileApi factoryMethod(int type) {
		switch (type) {
		case 1:
			return new ExportMysqlDBFile();
		case 2:
			return new ExportOracleDBFile();
		default:
			throw new IllegalArgumentException("不支持导出这种数据库文件");
		}
	}

}
