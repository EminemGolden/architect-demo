package com.tz.architect.export.lib;

/**
 * 创建者
 * 执行导出操作
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月4日
 * @version 1.0
 */
public abstract class ExportOperate {

	/**
	 * 工厂方法，用于创建导出数据的文件对象
	 * @return
	 */
	protected abstract ExportFileApi factoryMethod();
		
	
	public boolean export(String data){
		//导出特定文件
		ExportFileApi api = factoryMethod();
		return api.export(data);
	}
	
}
