

单例模式


配置文件（资源）

懒汉式、饿汉式、双重检查、枚举、静态内部类

本质：控制对象的个数

能不能控制对象个数为5个？
数据库连接池commons-dbcp database connection pool

Calendar.getInstance();

工厂方法模式

框架：做了部分工作，定义接口（jdbc）
框架只完成一定功能

导出数据的框架

不确定的：要导出什么文件不确定
确定的：

工厂方法模式定义：
定义一个用于创建对象的接口，让子类决定实例化哪一个类，
工厂方法使一个类的实例化延迟到创建器的子类中

工厂方法模式，比简单工厂扩展性更强

相关模式：抽象工厂模式，模板方法模式

Android中的应用：CursorFactory、LayoutInflater.Factory

<com.tz.MyView >










