package com.tz.ordersystem;

/**
 * 企业订单
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class EnterpriseOrder implements OrderApi {

	// 企业名称 J2EE
	private String enterpriseName;
	// 产品编号
	private String productId;
	// 产品数量
	private int orderProductNum;

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "企业订单 [enterpriseName=" + enterpriseName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}

}
