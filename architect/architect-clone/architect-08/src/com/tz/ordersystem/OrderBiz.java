package com.tz.ordersystem;

/**
 * 业务类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class OrderBiz {

	/**
	 * 创建订单（取决于产品的数量）
	 * @param order
	 */
	public void saveOrder(OrderApi order){
		while(order.getOrderProductNum() > 1000){
			//产品数量大于1000，拆分出新的订单对象
			OrderApi newOrder = null;
			if(order instanceof PersonalOrder){
				//将父类类型的引用转为子类的实际类型引用
				PersonalOrder p1 = (PersonalOrder) order;
				PersonalOrder p2 = new PersonalOrder();
				p2.setCustomerName(p1.getCustomerName());
				p2.setProductId(p1.getProductId());
				
				newOrder = p2;				
			}else if(order instanceof EnterpriseOrder){
				EnterpriseOrder e1 = (EnterpriseOrder) order;
				//生成新的订单对象
				EnterpriseOrder e2 = new EnterpriseOrder();
				//新的订单对象与原来的订单对象的差别只在于产品数量不一样
				//其他信息都一致
				e2.setEnterpriseName(e1.getEnterpriseName());
				e2.setProductId(e1.getProductId());
				
				newOrder = e2;
			}
			

			newOrder.setOrderProductNum(1000);
			order.setOrderProductNum(order.getOrderProductNum()-1000);
			
			//保存，其他业务功能，此处省略一万行代码
			System.out.println("拆分生成订单："+newOrder);
		}
		
		System.out.println("订单："+order);
	}
	
}
