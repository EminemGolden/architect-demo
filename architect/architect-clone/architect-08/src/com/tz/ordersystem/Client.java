package com.tz.ordersystem;

public class Client {

	public static void main(String[] args) {
		//个人订单对象
		PersonalOrder po = new PersonalOrder();
		po.setOrderProductNum(2999);
		po.setCustomerName("琅琊榜-胡歌");
		po.setProductId("P0002");
		
		//企业订单
		EnterpriseOrder eo = new EnterpriseOrder();
		eo.setOrderProductNum(3999);
		eo.setEnterpriseName("华为");
		eo.setProductId("P0008");
		
		//System.out.println(po);
		System.out.println(eo);
		OrderBiz biz = new OrderBiz();
		biz.saveOrder(eo);
		
	}

}
