package com.tz.prototype2;

/**
 * 个人订单
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class PersonalOrder implements OrderApi {

	// 客户名称
	private String customerName;
	// 产品编号
	private String productId;
	// 订单数量
	private int orderProductNum;
	
	@Override
	public OrderApi cloneOrder() {
		PersonalOrder order = new PersonalOrder();
		order.setCustomerName(customerName);
		order.setProductId(productId);
		order.setOrderProductNum(orderProductNum);
		return order;
	}

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "个人订单 [customerName=" + customerName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}


}
