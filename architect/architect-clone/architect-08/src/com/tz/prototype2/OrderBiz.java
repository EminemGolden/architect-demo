package com.tz.prototype2;

/**
 * 业务类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class OrderBiz {

	/**
	 * 创建订单（取决于产品的数量）
	 * @param order
	 */
	public void saveOrder(OrderApi order){
		while(order.getOrderProductNum() > 1000){
			//产品数量大于1000，拆分出新的订单对象
			OrderApi newOrder = order.cloneOrder();
			newOrder.setOrderProductNum(1000);
			order.setOrderProductNum(order.getOrderProductNum()-1000);
			
			//保存，其他业务功能，此处省略一万行代码
			System.out.println("拆分生成订单："+newOrder);
		}
		
		System.out.println("订单："+order);
	}
	
}
