package com.tz.prototype2;

/**
 * 大客户订单
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class VIPOrder implements OrderApi {
	// 客户名称
	private String customerName;
	// 产品编号
	private String productId;
	// 订单数量
	private int orderProductNum;

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	@Override
	public void setOrderProductNum(int num) {
		this.orderProductNum = num;
	}

	@Override
	public OrderApi cloneOrder() {
		VIPOrder order = new VIPOrder();
		order.setCustomerName(customerName);
		order.setOrderProductNum(orderProductNum);
		order.setProductId(productId);
		return order;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "大客户订单 [customerName=" + customerName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}

	
}
