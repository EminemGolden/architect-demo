package com.tz.prototype3;

/**
 * 订单接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public interface OrderApi {

	/**
	 * 获取订单产品的数量
	 * @return
	 */
	int getOrderProductNum();
	
	void setOrderProductNum(int num);
	
	OrderApi cloneOrder();
	
}
