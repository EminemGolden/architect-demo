package com.tz.prototype3;

/**
 * 企业订单
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class EnterpriseOrder implements OrderApi {

	// 企业名称 J2EE
	private String enterpriseName;
	// 产品
	private Product product;
	// 产品数量
	private int orderProductNum;

	@Override
	public OrderApi cloneOrder() {
		EnterpriseOrder order = new EnterpriseOrder();
		order.setEnterpriseName(enterpriseName);
		try {
			order.setProduct((Product)product.clone());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		order.setOrderProductNum(orderProductNum);
		return order;
	}

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "企业订单 [enterpriseName=" + enterpriseName + ", Product=" +product.hashCode()+","+ product + ", orderProductNum="
				+ orderProductNum + "]";
	}

	

}
