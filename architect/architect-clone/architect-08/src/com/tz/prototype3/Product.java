package com.tz.prototype3;

/**
 * 产品
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public class Product implements Cloneable {

	// 产品名称
	private String name;
	// 产品编号
	private String productId;
	
	//
	//Maker maker;

	@Override
	protected Object clone() throws CloneNotSupportedException {
		//Product product = new Product();
		//product.setMaker((Maker)marker.clone());
		
		return super.clone();
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", productId=" + productId + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

}
