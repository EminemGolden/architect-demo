package com.tz.prototype;

public class ConcretePrototype1 implements Prototype{

	@Override
	public Prototype cloneme() {
		Prototype prototype = new ConcretePrototype1();
		return prototype;
	}

}
