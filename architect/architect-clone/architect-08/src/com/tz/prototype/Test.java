package com.tz.prototype;

public class Test {

	public static void main(String[] args) {
		Prototype p = new ConcretePrototype1();
		
		Client client = new Client(p);
		client.operation();
	}

}
