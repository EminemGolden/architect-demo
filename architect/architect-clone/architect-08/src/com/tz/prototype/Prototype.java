package com.tz.prototype;

/**
 * 原型接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月14日
 * @version 1.0
 */
public interface Prototype {

	/**
	 * 克隆自身的方法
	 * @return
	 */
	Prototype cloneme();
	
}
