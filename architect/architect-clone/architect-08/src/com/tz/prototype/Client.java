package com.tz.prototype;

public class Client {

	private Prototype prototype;

	public Client(Prototype prototype) {
		super();
		this.prototype = prototype;
	}

	public void operation(){
		//创建原型接口的对象
		Prototype prototype = this.prototype.cloneme();
	}
	
}
