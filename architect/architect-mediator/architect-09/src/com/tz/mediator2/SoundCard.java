package com.tz.mediator2;

/**
 * 
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class SoundCard extends Colleague {

	public SoundCard(Mediator mediator) {
		super(mediator);
	}

	/**
	 * 播放声音
	 * @param data
	 */
	public void playSound(String data){
		System.out.println("声音："+data);
	}
	
}
