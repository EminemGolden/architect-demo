package com.tz.mediator2;

/**
 * The secretary of The king of the country  这个国家的国王的秘书
 * 同事类（所有立足于主板上需要交互的电脑组件的父类）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public abstract class Colleague {

	private Mediator mediator;
	
	public Colleague(Mediator mediator){
		this.mediator = mediator;
	}
	
	public Mediator getMediator() {
		return mediator;
	}
	
}
