package com.tz.mediator2;

/**
 * 中介者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public interface Mediator {

	/**
	 * 当同事类发生改变时，调用次方法，此方法中会根据实际情况与相应的其它同事进行交互
	 * @param colleague
	 */
	void changed(Colleague colleague);
	
}
