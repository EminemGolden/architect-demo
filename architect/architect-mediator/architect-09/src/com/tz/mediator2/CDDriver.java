package com.tz.mediator2;

/**
 * 光驱类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class CDDriver extends Colleague {

	private String data;
	
	public CDDriver(Mediator mediator) {
		super(mediator);
	}
	
	
	/**
	 * 读取光盘
	 */
	public void readCD(){
		this.data = "设计模式是神马,是前人经验的总结";
		//读到数据，通知主板
		this.getMediator().changed(this);
	}
	
	
	public String getData() {
		return data;
	}

}
