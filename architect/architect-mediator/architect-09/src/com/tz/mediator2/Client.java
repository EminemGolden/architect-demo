package com.tz.mediator2;

public class Client {

	public static void main(String[] args) {
		//中介者，主板
		MainBoard mediator = new MainBoard();
		
		//同事类
		CDDriver cd = new CDDriver(mediator);
		CPU cpu = new CPU(mediator);
		VideoCard vc = new VideoCard(mediator);
		SoundCard sc = new SoundCard(mediator);
		
		mediator.setCdDriver(cd);
		mediator.setCpu(cpu);
		mediator.setVideoCard(vc);
		mediator.setSoundCard(sc);
		
		//光驱读数据
		cd.readCD();
		
	}

}
