package com.tz.mediator2;

/**
 * 显卡，同事类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class VideoCard extends Colleague {

	public VideoCard(Mediator mediator) {
		super(mediator);
	}
	
	/**
	 * 显示视频数据
	 * @param data
	 */
	public void showData(String data){
		System.out.println("您正在观看："+data);
	}

}
