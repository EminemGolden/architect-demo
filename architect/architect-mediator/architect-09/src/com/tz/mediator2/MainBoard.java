package com.tz.mediator2;

/**
 * 主板，中介者的具体实现，负责与所有同事类进行交互
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class MainBoard implements Mediator {

	private CDDriver cdDriver;
	private CPU cpu;
	private VideoCard videoCard;
	private SoundCard soundCard;

	@Override
	public void changed(Colleague colleague) {
		//光驱读到数据了
		if(colleague == cdDriver){
			cdDriverReadData(cdDriver);
		}
		else if(colleague == cpu){
			//CPU把数据处理完成
			cpuResolveData(cpu);
		}
		
	}
	
	private void cdDriverReadData(CDDriver cd){
		//获取光驱读到的数据，给CPU进行处理
		String data = cd.getData();
		this.cpu.resolveData(data);
	}
	
	private void cpuResolveData(CPU cpu){
		//主板获取CPU处理得到的视频和音频数据
		String videoData = cpu.getVideoData();
		String soundData = cpu.getSoundData();
		
		//主板分别把这些数据给了显卡和声卡
		this.videoCard.showData(videoData);
		this.soundCard.playSound(soundData);
		
	}
	

	public void setCdDriver(CDDriver cdDriver) {
		this.cdDriver = cdDriver;
	}

	public void setCpu(CPU cpu) {
		this.cpu = cpu;
	}

	public void setVideoCard(VideoCard videoCard) {
		this.videoCard = videoCard;
	}

	public void setSoundCard(SoundCard soundCard) {
		this.soundCard = soundCard;
	}

}
