package com.tz.mediator2;

/**
 * CPU同事类
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class CPU extends Colleague {

	// 视频数据
	private String videoData;
	// 音频数据
	private String soundData;

	public CPU(Mediator mediator) {
		super(mediator);
	}

	/**
	 * 处理数据，把数据分成音频和视频的数据
	 * @param data
	 */
	public void resolveData(String data) {
		//拆分数据
		String[] ss = data.split(",");
		this.videoData = ss[0];
		this.soundData = ss[1];
		
		//通知主板
		this.getMediator().changed(this);
	}

	public String getVideoData() {
		return videoData;
	}

	public String getSoundData() {
		return soundData;
	}

}
