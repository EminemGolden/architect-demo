package com.tz.mediator3;

/**
 * 描述部门和人员关系的类
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class DepUserModel {

	// 编号（主键）
	private String id;
	// 部门
	private String depId;
	// 员工
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "DepUserModel [id=" + id + ", depId=" + depId + ", userId=" + userId + "]";
	}

}
