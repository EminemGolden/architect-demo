package com.tz.mediator3;

/**
 * 员工（同事）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class User {

	//人员编号
	private String userId;
	//人员名称
	private String userName;
	
	/**
	 * 离职
	 */
	public void dismiss(){
		DepUserMediator.getInstance().deleteUser(userId);
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
