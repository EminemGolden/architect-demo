package com.tz.mediator3;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 部门与用于交互的中介者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class DepUserMediator {
	//单例模式
	private static DepUserMediator mInstance = new DepUserMediator();

	private DepUserMediator(){
		init();
	}
	
	public static DepUserMediator getInstance(){
		return mInstance;
	}
	
	//所有部门的员工信息
	private Collection<DepUserModel> depUserCol = new ArrayList<DepUserModel>();
	
	//初始化数据
	private void init(){
		
		DepUserModel dum1 = new DepUserModel();
		dum1.setId("1");
		dum1.setDepId("DEP01");
		dum1.setUserId("U01");
		depUserCol.add(dum1);
		
		DepUserModel dum2 = new DepUserModel();
		dum2.setId("2");
		dum2.setDepId("DEP01");
		dum2.setUserId("U02");
		depUserCol.add(dum2);
		
		DepUserModel dum3 = new DepUserModel();
		dum3.setId("3");
		dum3.setDepId("DEP02");
		dum3.setUserId("U03");
		depUserCol.add(dum3);
		
		DepUserModel dum4 = new DepUserModel();
		dum4.setId("4");
		dum4.setDepId("DEP02");
		dum4.setUserId("U04");
		depUserCol.add(dum4);
		
		DepUserModel dum5 = new DepUserModel();
		dum5.setId("5");
		dum5.setDepId("DEP02");
		dum5.setUserId("U05");
		depUserCol.add(dum5);
		
	}
	
	/**
	 * 撤销部门
	 * @param depId
	 */
	public void deleteDep(String depId){
		//临时集合，用于存放被撤销部门的所有员工信息
		Collection<DepUserModel> tmpCol = new ArrayList<DepUserModel>();
		for (DepUserModel dum : depUserCol) {
			if(dum.getDepId().equals(depId)){
				tmpCol.add(dum);
			}
		}
		//直接移除
		depUserCol.removeAll(tmpCol);
	}
	
	/**
	 * 删除用户
	 * @param userId
	 */
	public void deleteUser(String userId){
		Collection<DepUserModel> tmpCol = new ArrayList<DepUserModel>();
		for (DepUserModel dum : depUserCol) {
			if(dum.getUserId().equals(userId)){
				tmpCol.add(dum);
			}
		}
		//直接移除
		depUserCol.removeAll(tmpCol);
	}
	
	
	public void showUserDeps(){
		for (DepUserModel depUserModel : depUserCol) {
			System.out.println(depUserModel);
		}
	}
	
}
