package com.tz.mediator3;

public class Client {

	public static void main(String[] args) {
		//DepUserMediator mediator = DepUserMediator.getInstance();
		
		//这个员工离职
		User user = new User();
		user.setUserId("U01");
		user.dismiss();
		
		//mediator.showUserDeps();
		
		//撤销部门
		Department dep = new Department();
		dep.setDepId("DEP02");
		dep.deleteDep();
		
		System.out.println("--------------");
		//mediator.showUserDeps();
		
	}

}
