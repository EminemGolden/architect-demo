package com.tz.mediator3;

/**
 * 部门（同事）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class Department {

	// 部门ID
	private String depId;
	// 部门名称
	private String name;
	
	
	/**
	 * 撤销部门
	 */
	public void deleteDep(){
		DepUserMediator.getInstance().deleteDep(depId);
	}
	

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
