package com.tz.mediator;

/**
 * 具体的同事类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class ConcreteColleagueB extends Colleague{

	public ConcreteColleagueB(Mediator mediator) {
		super(mediator);
	}

	public void someOperation(){
		getMediator().changed(this);
	}
}
