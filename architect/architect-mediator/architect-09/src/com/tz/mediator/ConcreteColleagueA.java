package com.tz.mediator;

/**
 * 具体的同事类
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class ConcreteColleagueA extends Colleague{

	public ConcreteColleagueA(Mediator mediator) {
		super(mediator);
	}
	
	/**
	 * 示意方法，执行某些业务功能
	 */
	public void someOperation(){
		getMediator().changed(this);
	}

}
