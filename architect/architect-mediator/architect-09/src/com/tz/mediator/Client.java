package com.tz.mediator;

public class Client {

	public static void main(String[] args) {
		//中介者
		ConcreteMediator mediator = new ConcreteMediator();
		
		//同事A
		ConcreteColleagueA colleagueA = new ConcreteColleagueA(mediator);
		
		//同事B
		ConcreteColleagueB colleagueB = new ConcreteColleagueB(mediator);
		
		//中介者持有所有同事对象
		mediator.setColleagueA(colleagueA);
		mediator.setColleagueB(colleagueB);
		
		colleagueA.someOperation();
	}

}
