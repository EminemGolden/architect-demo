package com.tz.mediator;

/**
 * 中介者接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public interface Mediator {

	/**
	 * 同事对象在自身改变的时候来通知中介者的方法
	 * 让中介者去找相应的其他同事进行交互
	 * @param colleague
	 */
	void changed(Colleague colleague);
	
	
}
