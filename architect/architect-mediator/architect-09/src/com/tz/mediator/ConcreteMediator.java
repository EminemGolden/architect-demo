package com.tz.mediator;

/**
 * 具体的中介者实现
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public class ConcreteMediator implements Mediator {

	// 同事A
	private ConcreteColleagueA colleagueA;
	// 同事B
	private ConcreteColleagueB colleagueB;

	@Override
	public void changed(Colleague colleague) {
		//如果是A改变了，之后
		System.out.println("A--------------");
		//B开始做一些其他的事情
		System.out.println("B--------------");
	}

	public ConcreteColleagueA getColleagueA() {
		return colleagueA;
	}

	public void setColleagueA(ConcreteColleagueA colleagueA) {
		this.colleagueA = colleagueA;
	}

	public ConcreteColleagueB getColleagueB() {
		return colleagueB;
	}

	public void setColleagueB(ConcreteColleagueB colleagueB) {
		this.colleagueB = colleagueB;
	}

}
