package com.tz.mediator;

/**
 * 同事
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月16日
 * @version 1.0
 */
public abstract class Colleague {

	/**
	 * 持有中介者对象，每一个同事类都会持有中介者对象
	 */
	private Mediator mediator;
	
	/**
	 * 传入中介者对象
	 * @param mediator
	 */
	public Colleague(Mediator mediator){
		this.mediator = mediator;
	}
	
	public Mediator getMediator() {
		return mediator;
	}
	
}
