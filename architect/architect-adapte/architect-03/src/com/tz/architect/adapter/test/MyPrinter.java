package com.tz.architect.adapter.test;

import java.io.IOException;
import java.io.Reader;

public class MyPrinter {

	public void printText(Reader reader){
		char[] buf = new char[1024];
		try {
			reader.read(buf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

