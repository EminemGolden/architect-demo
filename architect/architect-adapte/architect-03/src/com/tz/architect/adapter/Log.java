package com.tz.architect.adapter;

import java.io.Serializable;

/**
 * 日志实体
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月2日
 * @version 1.0
 */
public class Log implements Serializable{

	private int id;
	private String content;
	private String time;

	public Log() {
	}

	public Log(int id, String content, String time) {
		super();
		this.id = id;
		this.content = content;
		this.time = time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Log [id=" + id + ", content=" + content + ", time=" + time + "]";
	}

}
