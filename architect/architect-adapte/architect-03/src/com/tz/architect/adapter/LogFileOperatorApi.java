package com.tz.architect.adapter;

import java.util.List;

/**
 * 日志文件操作接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月2日
 * @version 1.0
 */
public interface LogFileOperatorApi {

	/**
	 * 将日志写入文件中
	 * @param list
	 */
	void writeLogFile(List<Log> list);
	
	/**
	 * 读取日志文件中的日志
	 * @return
	 */
	List<Log> readLogFile();
	
}
