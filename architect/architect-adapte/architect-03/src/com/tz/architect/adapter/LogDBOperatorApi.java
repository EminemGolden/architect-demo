package com.tz.architect.adapter;

import java.util.List;

/**
 * 第二版，日志管理，支持数据库操作接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月2日
 * @version 1.0
 */
public interface LogDBOperatorApi {

	/**
	 * 添加日志
	 * @param log
	 */
	void insert(Log log);
	
	/**
	 * 删除日志
	 * @param log
	 */
	void delete(Log log);
	
	/**
	 * 获取日志
	 * @return
	 */
	List<Log> getAll();
	
}
