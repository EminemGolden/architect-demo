package com.tz.architect.adapter;

import java.util.ArrayList;
import java.util.List;

public class Client {

	public static void main(String[] args) {
		
		List<Log> list = new ArrayList<Log>();
		list.add(new Log(1,"存款","2015-11-20 21:20:12"));
		list.add(new Log(2,"取款","2015-11-20 21:20:12"));
		list.add(new Log(3,"转账","2015-11-20 21:20:12"));
		
		//第一个版本
		LogFileOperatorApi logFileApi = new LogFileOperatorImpl("d://log.txt");
		
		logFileApi.writeLogFile(list);
		
		System.out.println(logFileApi.readLogFile());
		
		
		//第二个版本
		//保存到数据库中
		//LogDBOperatorApi logDBApi = new LogDBOperatorImpl();
		//还可以保存到文件中
		LogDBOperatorApi logDBApi = new Adapter(logFileApi);
		logDBApi.insert(new Log(4,"还贷","2015-11-20 21:20:15"));
		
		//传，Adapter内部的实现是操作文件的接口实现
		printLog(logDBApi);
		
		
		//传,操作数据的接口的实现
		LogDBOperatorApi logDBApi2 = new LogDBOperatorImpl();
		printLog(logDBApi2);
		
		//结果：传入同一个接口，干的事情确实不同的
		//扩展了系统，重用了代码
	}
	
	
	/**
	 * 客户端的业务方法（木已成舟），打印日志
	 * @param api
	 */
	public static void printLog(LogDBOperatorApi api){
		System.out.println(api);
	}

}
