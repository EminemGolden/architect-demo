package com.tz.architect.adapter.second;

public interface Target {

	void request();
	
}
