package com.tz.architect.adapter.second;

public class Client {

	public static void main(String[] args) {
		
		//Adapter将Adaptee是配成了Client需要的Target
		Adaptee adaptee = new AdapteeImpl();
		
		Target target = new Adapter(adaptee);

	}

}
