package com.tz.architect.adapter;

import java.util.List;

/**
 * LogDBOperatorApi接口支持文件和数据库操作
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月2日
 * @version 1.0
 */
public class Adapter implements LogDBOperatorApi {

	private LogFileOperatorApi adaptee;
	
	
	public Adapter(LogFileOperatorApi adaptee) {
		super();
		this.adaptee = adaptee;
	}

	@Override
	public void insert(Log log) {
		List<Log> list = adaptee.readLogFile();
		list.add(log);
		adaptee.writeLogFile(list);
	}

	@Override
	public void delete(Log log) {
		List<Log> list = adaptee.readLogFile();
		list.remove(log);
		adaptee.writeLogFile(list);
	}

	@Override
	public List<Log> getAll() {
		return adaptee.readLogFile();
	}

}
