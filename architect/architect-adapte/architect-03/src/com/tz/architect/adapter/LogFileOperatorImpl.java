package com.tz.architect.adapter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * 日志文件操作实现类
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月2日
 * @version 1.0
 */
public class LogFileOperatorImpl implements LogFileOperatorApi {

	// 日志文件的路径
	private String filepath;

	public LogFileOperatorImpl(String filepath) {
		super();
		this.filepath = filepath;
	}

	@Override
	public void writeLogFile(List<Log> list) {
		File file = new File(this.filepath);
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			os.writeObject(list);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Log> readLogFile() {
		File file = new File(this.filepath);
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
			return (List<Log>) is.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

}
