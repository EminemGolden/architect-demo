package com.tz.architect.adapter.second;

public interface Adaptee {

	void specificRequest();
	
}
