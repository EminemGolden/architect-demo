package com.jason.composite;

/**
 * 叶子
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Leaf {

	/**
	 * 叶子对象的名称
	 */
	private String name;
	
	public Leaf(String name){
		this.name = name;
	}
	
	/**
	 * 输出
	 * @param preStr
	 */
	public void print(String preStr){
		System.out.println(preStr+"-"+name);
	}
	
}
