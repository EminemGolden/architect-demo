package com.jason.composite;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 组合
 * 
 * @author Jason QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Composite {

	/**
	 * 组合集合
	 */
	private Collection<Composite> childComposite = new ArrayList<Composite>();

	/**
	 * 叶子集合
	 */
	private Collection<Leaf> childLeaf = new ArrayList<Leaf>();

	// 组合对象的名称
	private String name;

	public Composite(String name) {
		super();
		this.name = name;
	}
	
	/**
	 * 添加组合对象
	 * @param c
	 */
	public void addComposite(Composite c){
		childComposite.add(c);
	}
	
	/**
	 * 添加叶子对象
	 * @param leaf
	 */
	public void addLeaf(Leaf leaf){
		childLeaf.add(leaf);
	}
	
	/**
	 * 打印
	 * @param preStr
	 */
	public void print(String preStr){
		System.out.println(preStr+"+"+this.name);
		//叶子对象
		preStr += " ";
		for (Leaf leaf : childLeaf) {
			leaf.print(preStr);
		}
		
		//子组合对象
		for (Composite c : childComposite) {
			c.print(preStr);
		}
	}

}
