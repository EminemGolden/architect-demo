package com.jason.composite2;



public class Client {

	public static void main(String[] args) {
		//根节点
		Component root = new Composite();
		//组合对象
		Component c1 = new Composite();
		Component c2 = new Composite();
		
		//单个对象
		Component leaf1 = new Leaf();
		Component leaf2 = new Leaf();
		Component leaf3 = new Leaf();
		Component leaf4 = new Leaf();
		
		root.addChild(c1);
		root.addChild(c2);
		
		c1.addChild(leaf1);
		c1.addChild(leaf2);
		c1.addChild(leaf3);
		c1.addChild(leaf4);
		
	}

}
