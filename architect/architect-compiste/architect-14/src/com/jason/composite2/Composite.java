package com.jason.composite2;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合对象
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Composite extends Component {

	private List<Component> childComponents = new ArrayList<Component>(); 
	
	@Override
	public void someOperation() {
		for (Component c : childComponents) {
			c.someOperation();
		}
	}
	
	@Override
	public void addChild(Component child) {
		childComponents.add(child);
	}
	
	@Override
	public void remove(Component child) {
		childComponents.remove(child);
	}
	
	@Override
	public Component getChildren(int index) {
		return childComponents.get(index);
	}

}
