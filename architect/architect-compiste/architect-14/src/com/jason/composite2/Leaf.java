package com.jason.composite2;

/**
 * 单个对象
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Leaf extends Component {

	@Override
	public void someOperation() {
		System.out.println("----");
	}

}
