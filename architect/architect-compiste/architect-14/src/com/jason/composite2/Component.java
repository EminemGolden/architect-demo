package com.jason.composite2;

/**
 * 抽象组件类，组合对象以及单个对象，都要实现这个类
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public abstract class Component {

	/**
	 * 添加子节点
	 * @param child
	 */
	public void addChild(Component child){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 删除子节点
	 * @param child
	 */
	public void remove(Component child){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 获取某一个子节点
	 * @param index
	 * @return
	 */
	public Component getChildren(int index){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 其他的操作
	 */
	public abstract void someOperation();
	
}
