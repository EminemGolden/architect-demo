package com.jason.composite3;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合对象
 * 
 * @author Jason QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Composite extends Component {

	private String name;
	//组件集合
	private List<Component> childComponent = new ArrayList<Component>();

	public Composite(String name) {
		super();
		this.name = name;
	}
	
	@Override
	public void addChild(Component child) {
		childComponent.add(child);
	}
	
	@Override
	public void remove(Component child) {
		childComponent.remove(child);
	}
	
	@Override
	public Component getChildren(int index) {
		return childComponent.get(index);
	}
	

	@Override
	public void print(String preStr) {
		//输出自己
		System.out.println(preStr+"+"+this.name);
		//缩进
		preStr += " ";
		for (Component c : childComponent) {
			c.print(preStr);
		}
	}

}
