package com.jason.composite3;

import com.jason.composite3.Component;

/**
 * 单个对象
 * 
 * @author Jason QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public class Leaf extends Component {

	private String name;

	public Leaf(String name) {
		super();
		this.name = name;
	}

	@Override
	public void print(String preStr) {
		System.out.println(preStr+"-"+name);
	}

}
