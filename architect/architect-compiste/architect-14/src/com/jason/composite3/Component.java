package com.jason.composite3;


/**
 * 抽象组件类
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月4日
 * @version 1.0
 */
public abstract class Component {

	/**
	 * 添加子节点
	 * @param child
	 */
	public void addChild(Component child){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 删除子节点
	 * @param child
	 */
	public void remove(Component child){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 获取某一个子节点
	 * @param index
	 * @return
	 */
	public Component getChildren(int index){
		throw new UnsupportedOperationException("对象不支持这个功能");
	}
	
	/**
	 * 打印
	 * @param preStr
	 */
	public abstract void print(String preStr);
	
}
