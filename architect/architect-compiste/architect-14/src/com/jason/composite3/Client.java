package com.jason.composite3;

public class Client {

	public static void main(String[] args) {
		//根节点（组合对象）
		Component root = new Composite("服装");
	
		//组合对象
		Component c1 = new Composite("男装");
		Component c2 = new Composite("女装");
		
		//单个对象
		Component lf1 = new Leaf("衬衣");
		Component lf2 = new Leaf("夹克");
		Component lf3 = new Leaf("裙子");
		Component lf4 = new Leaf("套装");
		
		root.addChild(c1);
		root.addChild(c2);
		
		c1.addChild(lf1);
		c1.addChild(lf2);
		
		c2.addChild(lf3);
		c2.addChild(lf4);
		
		root.print("");
		
	}
	

}
