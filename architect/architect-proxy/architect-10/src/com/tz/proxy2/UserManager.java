package com.tz.proxy2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.tz.info.DBUtils;

public class UserManager {

	/**
	 * 根据部门id查询所有的员工
	 * @param depId
	 * @return
	 * @throws Exception 
	 */
	public Collection<UserModelApi> getUserByDepId(String depId) throws Exception{
		Collection<UserModelApi> col = new ArrayList<UserModelApi>() ;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBUtils.getConnection();
			String sql = " select * from tbl_user u, tbl_dep d where u.depid = d.depid and d.depid like ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, depId+"%");
			rs = ps.executeQuery();
			while(rs.next()){
				//使用UserModel的代理对象
				UserModel um = new UserModel();
				Proxy proxy = new Proxy(um);
				//为了节省内存，只查询最常用的两个字段
				proxy.setUserId(rs.getString("userid"));
				proxy.setName(rs.getString("name"));
				col.add(proxy);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtils.close(conn, ps, rs);
		}
		
		return col;
	}
	
}
