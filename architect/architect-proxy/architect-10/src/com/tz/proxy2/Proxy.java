package com.tz.proxy2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tz.info.DBUtils;

/**
 * 代理对象
 * 绝大多数情况下，只需要查看用户的id和name属性值
 * 当需要查看部门id，sex时，我们再临时加载这些数据
 * 目的：节省内存
 * @author Jason QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public class Proxy implements UserModelApi {

	// 持有具体的目标对象
	private UserModel realSubject;

	public Proxy(UserModel realSubject) {
		super();
		this.realSubject = realSubject;
	}

	@Override
	public String getUserId() {
		return realSubject.getUserId();
	}

	@Override
	public void setUserId(String userId) {
		realSubject.setUserId(userId);
	}

	@Override
	public String getName() {
		return realSubject.getName();
	}

	@Override
	public void setName(String name) {
		realSubject.setName(name);
	}

	@Override
	public String getDepId() {
		if(realSubject.getDepId() == null){
			reload();
		}
		return realSubject.getDepId();
	}

	@Override
	public void setDepId(String depId) {
		realSubject.setDepId(depId);
	}

	@Override
	public String getSex() {
		if(realSubject.getSex() == null){
			reload();
		}
		return realSubject.getSex();
	}

	@Override
	public void setSex(String sex) {
		realSubject.setSex(sex);
	}
	
	/**
	 * 重新查询数据库以获取完整的用户数据
	 */
	private void reload(){
		System.out.println("重新查询...");
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//根据用户的id，查询其他字段
			conn = DBUtils.getConnection();
			String sql = " select * from tbl_user where userid = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, realSubject.getUserId());
			rs = ps.executeQuery();
			if(rs.next()){
				realSubject.setDepId(rs.getString("depid"));
				realSubject.setSex(rs.getString("sex"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			DBUtils.close(conn, ps, rs);
		}
		
	}

	@Override
	public String toString() {
		return "Proxy [userId=" + getUserId() + ", name=" + getName() +  "]";
	}
}
