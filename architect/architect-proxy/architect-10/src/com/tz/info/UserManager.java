package com.tz.info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class UserManager {

	/**
	 * 根据部门id查询所有的员工
	 * @param depId
	 * @return
	 * @throws Exception 
	 */
	public Collection<UserModel> getUserByDepId(String depId) throws Exception{
		Collection<UserModel> col = new ArrayList<UserModel>() ;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBUtils.getConnection();
			String sql = " select * from tbl_user u, tbl_dep d where u.depid = d.depid and d.depid like ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, depId+"%");
			rs = ps.executeQuery();
			while(rs.next()){
				UserModel user = new UserModel();
				user.setUserId(rs.getString("userid"));
				user.setName(rs.getString("name"));
				user.setDepId(rs.getString("depid"));
				user.setSex(rs.getString("sex"));
				col.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtils.close(conn, ps, rs);
		}
		
		return col;
	}
	
}
