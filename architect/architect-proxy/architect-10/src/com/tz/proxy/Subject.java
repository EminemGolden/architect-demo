package com.tz.proxy;

/**
 * 目标接口，具体的目标对象和代理都会实现这个接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public interface Subject {

	/**
	 * 示意方法，请求
	 */
	void request();
	
}
