package com.jason.strategy5;

/**
 * 支付策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public interface PaymentStrategy {

	/**
	 * 支付算法
	 * @param context 支付上下文，包含算法所需的数据
	 */
	void pay(PaymentContext context);
	
}
