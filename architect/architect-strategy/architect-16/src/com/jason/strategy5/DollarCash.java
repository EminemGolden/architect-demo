package com.jason.strategy5;

/**
 * 美元现金支付策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class DollarCash implements PaymentStrategy {

	@Override
	public void pay(PaymentContext context) {
		System.out.println("现在给："+context.getUsername()+"支付美元现金："+context.getMoney()+"元");
	}

}
