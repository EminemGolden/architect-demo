package com.jason.strategy5;

/**
 * 带支付卡号的支付上下文
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class PaymentContext2 extends PaymentContext {

	//支付卡号
	private String cardNo;
	
	public PaymentContext2(String username, double money, PaymentStrategy strategy,String cardNo) {
		super(username, money, strategy);
		this.cardNo = cardNo;
	}
	
	public String getCardNo() {
		return cardNo;
	}

}
