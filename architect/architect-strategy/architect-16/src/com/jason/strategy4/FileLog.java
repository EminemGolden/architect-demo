package com.jason.strategy4;

/**
 * 文件日志策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class FileLog extends LogStrategyTemplate {

	@Override
	public void doLog(String msg) {
		System.out.println("把："+msg+"保存到文件");
	}

}
