package com.jason.strategy4;

/**
 * 日志策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public interface LogStrategy {

	/**
	 * 记录日志
	 * @param msg
	 */
	void doLog(String msg);
	
}
