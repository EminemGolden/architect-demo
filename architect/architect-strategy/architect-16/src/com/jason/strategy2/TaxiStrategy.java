package com.jason.strategy2;

/**
 * 出租车出行策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class TaxiStrategy implements CalculateStrategy {

	@Override
	public int calculatePrice(int km) {
		return km * 2;
	}

}
