package com.jason.strategy2;

/**
 * 公交出行策略
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class BusStrategy implements CalculateStrategy {

	@Override
	public int calculatePrice(int km) {
		if(km<=10){
			return 1;
		}else{
			int d = km - 10;
			int y = d % 5;
			int total = 1 + (d / 5) * 1;
			if(y > 0){
				total +=1;
			}
			return total;
		}
	}

}
