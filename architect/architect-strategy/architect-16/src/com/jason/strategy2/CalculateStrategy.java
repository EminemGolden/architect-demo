package com.jason.strategy2;

/**
 * 计算接口
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public interface CalculateStrategy {

	/**
	 * 计算费用
	 * @param km
	 * @return
	 */
	int calculatePrice(int km);
	
}
