package com.jason.strategy2;

/**
 * Context上下文
 * 
 * @author Jason QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class TrafficCalculator {

	private CalculateStrategy strategy;

	/**
	 * 计算
	 * @param km
	 * @return
	 */
	public int calculate(int km){
		if(strategy == null){
			strategy = new BusStrategy();
		}
		return strategy.calculatePrice(km);
	}
	
	
	public void setStrategy(CalculateStrategy strategy) {
		this.strategy = strategy;
	}
	
}
