package com.jason.strategy;

/**
 * 策略实现
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public class ConcreteStrategyB implements Strategy {

	@Override
	public void algorithm() {
		System.out.println("算法B");
	}

}
