package com.jason.strategy;

/**
 * 策略接口
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月8日
 * @version 1.0
 */
public interface Strategy {

	void algorithm();
	
}
