package com.tz.proxy;

/**
 * 代理对象
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public class Proxy implements Subject {

	// 持有被代理的对象
	private RealSubject subject;

	public Proxy(RealSubject subject) {
		super();
		this.subject = subject;
	}

	@Override
	public void request() {
		//在调用具体的目标对象的方法前，可以执行一些功能
		
		//调用具体的目标对象方法
		subject.request();
		
		//在调用具体的目标对象的方法后，可以执行一些功能
		
	}

}
