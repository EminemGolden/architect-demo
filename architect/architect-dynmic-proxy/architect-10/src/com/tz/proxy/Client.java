package com.tz.proxy;

public class Client {

	public static void main(String[] args) {
		//具体的目标对象
		RealSubject subject = new RealSubject();
		
		//代理对象
		Subject proxy = new Proxy(subject);
		
		proxy.request();
	}
	
}
