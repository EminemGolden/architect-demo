package com.tz.proxy;

/**
 * 具体的目标对象（需要被代理的对象）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public class RealSubject implements Subject{

	@Override
	public void request() {
		//.....
		System.out.println("============");
	}

}
