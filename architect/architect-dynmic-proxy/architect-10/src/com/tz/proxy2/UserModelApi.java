package com.tz.proxy2;

/**
 * 目标接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public interface UserModelApi {

	public String getUserId();

	public void setUserId(String userId);

	public String getName();

	public void setName(String name);

	public String getDepId();

	public void setDepId(String depId);

	public String getSex();

	public void setSex(String sex);
	
}
