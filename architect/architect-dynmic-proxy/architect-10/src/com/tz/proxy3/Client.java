package com.tz.proxy3;

public class Client {

	public static void main(String[] args) {
		Order order = new Order("性感丝袜", 100, "黄海波");
		OrderApi proxy = new OrderProxy(order);
		
		System.out.println(proxy.getProductName());
		
		proxy.setOrderNum(150, "蜜蜜");
		
		System.out.println("订单："+order);
	}

}
