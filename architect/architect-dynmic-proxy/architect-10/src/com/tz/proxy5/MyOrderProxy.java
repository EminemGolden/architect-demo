package com.tz.proxy5;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 在动态代理中原本这个类由虚拟机自动产生，这是我们通过静态代理的方式，手动实现的这个类
 * 订单代理（Proxy）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月21日
 * @version 1.0
 */
public class MyOrderProxy implements OrderApi {

	// 目标对象
	private Order order;
		
	private InvocationHandler handler;

	public MyOrderProxy(Order order,InvocationHandler handler) {
		super();
		this.order = order;
		this.handler = handler;
	}

	@Override
	public String getProductName() {
		try {
			Method method = order.getClass().getMethod("getProductName", null);
			return (String)handler.invoke(order, method, null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void setProductName(String productName, String user) {
		try {
			//方法的参数类型
			Class<?>[] artTypes = new Class[]{String.class,String.class};
			Method method = order.getClass().getMethod("setProductName",artTypes);
			//执行方法所需的参数
			Object[] args = new Object[]{productName,user}; 
			handler.invoke(order, method, args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getOrderNum() {
		try {
			Method method = order.getClass().getMethod("getOrderNum", null);
			return (Integer)handler.invoke(order, method, null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void setOrderNum(int orderNum, String user) {
		try {
			//方法的参数类型
			Class<?>[] artTypes = new Class[]{int.class,String.class};
			Method method = order.getClass().getMethod("setOrderNum",artTypes);
			//执行方法所需的参数
			Object[] args = new Object[]{orderNum,user}; 
			handler.invoke(order, method, args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getOrderUser() {
		try {
			Method method = order.getClass().getMethod("getOrderUser", null);
			return (String)handler.invoke(order, method, null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void setOrderUser(String orderUser, String user) {
		try {
			//方法的参数类型
			Class<?>[] artTypes = new Class[]{String.class,String.class};
			Method method = order.getClass().getMethod("setOrderUser",artTypes);
			//执行方法所需的参数
			Object[] args = new Object[]{orderUser,user}; 
			handler.invoke(order, method, args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
