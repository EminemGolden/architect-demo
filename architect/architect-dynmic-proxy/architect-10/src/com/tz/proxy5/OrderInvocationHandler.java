package com.tz.proxy5;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理句柄
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月21日
 * @version 1.0
 */
public class OrderInvocationHandler implements InvocationHandler{

	//订单（目标对象）
	private OrderApi order;
	
	/**
	 * method 目标对象要执行的方法
	 * args 目标对象要执行的方法的参数列表
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		//System.out.println(proxy);
		//只要是set开头的方法，进行权限检查
		if(method.getName().startsWith("set")){
			if(order.getOrderUser() != null && order.getOrderUser().equals(args[1])){
				//可以操作
				return method.invoke(order, args);
			}else{
				throw new IllegalAccessException("没有权限！");
			}
		}
		return method.invoke(order, args);
	}

	/**
	 * 动态代理所代理的对象（目标对象）
	 * @param order
	 */
	public void setTarget(OrderApi order){
		this.order = order;
	}
	
}
