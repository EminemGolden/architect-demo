package com.tz.info;

import java.util.Collection;

public class Client {

	public static void main(String[] args) {
		UserManager manager = new UserManager();
		try {
			Collection<UserModel> list = manager.getUserByDepId("0102");
			for (UserModel u : list) {
				System.out.println(u);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
