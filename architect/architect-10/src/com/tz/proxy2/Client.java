package com.tz.proxy2;

import java.util.ArrayList;

public class Client {

	public static void main(String[] args) {
		UserManager manager = new UserManager();
		try {
			ArrayList<UserModelApi> list = (ArrayList<UserModelApi>) manager.getUserByDepId("0101");
			for (UserModelApi u : list) {
				System.out.println(u);
			}
			
			System.out.println("--------------");
			UserModelApi user = list.get(0);
			System.out.println(user.getDepId()+","+user.getSex());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
