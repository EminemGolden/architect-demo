package com.tz.proxy2;

/**
 * 具体的目标对象（需要被代理的对象）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月18日
 * @version 1.0
 */
public class UserModel implements UserModelApi{

	// 用户编号
	private String userId;
	// 用户姓名
	private String name;
	// 部门编号
	private String depId;
	// 性别
	private String sex;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "UserModel [userId=" + userId + ", name=" + name + ", depId=" + depId + ", sex=" + sex + "]";
	}

}
