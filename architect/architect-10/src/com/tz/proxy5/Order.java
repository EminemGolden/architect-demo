package com.tz.proxy5;

/**
 * 订单（ConcreteSubject）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月21日
 * @version 1.0
 */
public class Order implements OrderApi {

	// 产品名称
	private String productName;
	// 订购的数量
	private int orderNum;
	// 创建订单的人员
	private String orderUser;

	public Order(String productName, int orderNum, String orderUser) {
		super();
		this.productName = productName;
		this.orderNum = orderNum;
		this.orderUser = orderUser;
	}

	@Override
	public String getProductName() {
		return this.productName;
	}

	@Override
	public void setProductName(String productName, String user) {
		this.productName = productName;
	}

	@Override
	public int getOrderNum() {
		return orderNum;
	}

	@Override
	public void setOrderNum(int orderNum, String user) {
		this.orderNum = orderNum;
	}

	@Override
	public String getOrderUser() {
		return orderUser;
	}

	@Override
	public void setOrderUser(String orderUser, String user) {
		this.orderUser = orderUser;
	}

	@Override
	public String toString() {
		return "Order [productName=" + productName + ", orderNum=" + orderNum + ", orderUser=" + orderUser + "]";
	}

}
