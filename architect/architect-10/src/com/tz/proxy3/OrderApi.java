package com.tz.proxy3;

/**
 * 订单对象接口（Suject）
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月21日
 * @version 1.0
 */
public interface OrderApi {

	/**
	 * 获取产品名称
	 * @return
	 */
	String getProductName();
	
	/**
	 * 设置订单订购的产品名称
	 * @param productName
	 * @param user 操作人员
	 */
	void setProductName(String productName,String user);
	
	/**
	 * 获取订单订购的数量
	 * @return
	 */
	int getOrderNum();
	
	/**
	 * 设置订单订购的数量
	 * @param orderNum
	 * @param user 操作人员
	 */
	void setOrderNum(int orderNum,String user);
	
	/**
	 * 获取创建订单的人员
	 * @return
	 */
	String getOrderUser();
	
	/**
	 * 设置创建订单的人员
	 * @param orderUser 创建订单的人员
	 * @param user 操作人员
	 */
	void setOrderUser(String orderUser,String user);
	
}
