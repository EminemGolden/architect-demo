package com.tz.proxy3;

/**
 * 订单代理（Proxy）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月21日
 * @version 1.0
 */
public class OrderProxy implements OrderApi {

	// 目标对象
	private Order order;
		

	public OrderProxy(Order order) {
		super();
		this.order = order;
	}

	@Override
	public String getProductName() {
		return order.getProductName();
	}

	@Override
	public void setProductName(String productName, String user) {
		//只有订单的创建者，才能够修改
		if(user != null && user.equals(this.order.getOrderUser())){
			order.setProductName(productName, user);
		}else{
			throw new IllegalAccessError("没有操作权限！");
		}
	}

	@Override
	public int getOrderNum() {
		return order.getOrderNum();
	}

	@Override
	public void setOrderNum(int orderNum, String user) {
		if(user != null && user.equals(this.order.getOrderUser())){
			order.setOrderNum(orderNum, user);
		}else{
			throw new IllegalAccessError("没有操作权限！");
		}
	}

	@Override
	public String getOrderUser() {
		return order.getOrderUser();
	}

	@Override
	public void setOrderUser(String orderUser, String user) {
		if(user != null && user.equals(this.order.getOrderUser())){
			order.setOrderUser(orderUser, user);
		}else{
			throw new IllegalAccessError("没有操作权限！");
		}
	}

}
