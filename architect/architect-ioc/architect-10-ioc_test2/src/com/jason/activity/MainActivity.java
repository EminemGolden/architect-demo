package com.jason.activity;

import com.jason.ioc.activity.BaseActivity;
import com.jason.ioc.annotation.ContentView;
import com.jason.ioc.annotation.OnClick;
import com.jason.ioc.annotation.OnLongClick;
import com.jason.ioc.annotation.ViewInject;

import android.widget.Button;
import android.widget.Toast;

@ContentView(R.layout.main)
public class MainActivity extends BaseActivity{

	@ViewInject(R.id.btn_forward)
	private Button btn_forward;

	@ViewInject(R.id.btn_back)
	private Button btn_back;
	
	private boolean flag;
	
	@OnClick({R.id.btn_forward,R.id.btn_back})
	public void mClick(Button btn){
		Toast.makeText(this, btn.getText().toString()+"点击我了！", 1).show();
	}
	
	
	@OnLongClick({R.id.btn_forward,R.id.btn_back})
	public boolean mClick2(Button btn){
		Toast.makeText(this, btn.getText().toString()+"长按我了！", 1).show();
		return true;
	}

	
	
}
