package com.jason.ioc.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import com.jason.ioc.annotation.ContentView;
import com.jason.ioc.annotation.EventBase;
import com.jason.ioc.annotation.ViewInject;
import com.jason.ioc.proxy.ListenerInvocationHandler;

import android.app.Activity;
import android.util.Log;
import android.view.View;

/**
 * 用于注入
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月23日
 * @version 1.0
 */
public class InjectUtils {
	
	private static final String METHOD_SET_CONTENT_VIEW = "setContentView";
	private static final String METHOD_FIND_VIEW_BY_ID = "findViewById";

	/**
	 * 注入（布局、控件、事件）
	 * @param activity 需要注入的类
	 */
	public static void inject(Activity activity){
		injectContentView(activity);
		injectViews(activity);
		injectEvents(activity);
	}
	
	/**
	 * 给控件注入事件
	 * @param activity
	 */
	private static void injectEvents(Activity activity) {
		try {
			Log.d("justin", "注入事件");
			//获取带有OnClick注解的方法
			Class<?> clazz = activity.getClass();
			Method[] methods = clazz.getDeclaredMethods();
			for (Method method : methods) {
				//我们要的是带有EventBase注解的注解
				Annotation[] annotations = method.getAnnotations();
				for (Annotation annotation : annotations) {
					//获取注解上的注解（有可能@Target、@Retention、@EventBase）
					Class<?> annotationType = annotation.annotationType();
					//指定注解上的注解类型EventBase
					EventBase eventBase = annotationType.getAnnotation(EventBase.class);
					//有可能没有EventBase注解
					if(eventBase == null){
						continue;
					}
					
					//有，获取事件3要素
					//设置监听的方法名称
					String listenerSetter = eventBase.listenerSetter();
					//监听接口
					Class<?> listenerType = eventBase.listenerType();
					//回调方法的名称
					String callBackMethod = eventBase.callBackMethod();
					//当callBackMethod方法执行时，实际要执行method
					Map<String, Method> methodMap = new HashMap<String, Method>();
					methodMap.put(callBackMethod, method);
					
					/**
					 * view.setOnClickListener(new View.OnClickListener() {
				
							@Override
							public void onClick(View v) {
							}
						}		
					 */
					//要给视图注册事件监听，首先要有视图，获取到view id再通过findViewById就可以拿到视图对象
					//@OnClick({R.id.btn_forward,R.id.btn_back})
					//注解的value方法
					Method valueMethod = annotationType.getDeclaredMethod("value");
					int[] viewIds = (int[])valueMethod.invoke(annotation);
					//获取viewIds所有的视图
					for (int viewId : viewIds) {
						View view = activity.findViewById(viewId);
						//给每一个view注册事件监听
						if(view == null){
							continue;
						}
						//获取设置事件监听方法
						Method setEventListenerMtd = view.getClass().getMethod(listenerSetter, listenerType);
						//执行
						//代理模式：控制访问
						//目标对象：事件监听对象
						//动态生成一个listenerType的代理对象
						ListenerInvocationHandler handler = new ListenerInvocationHandler(activity,methodMap);
						//new Class<?>[]{listenerType} 有什么接口，动态代理对象就会实现什么接口
						//也就是说，传什么接口，就可以成为什么类型的代理对象
						//经典总结：Java动态代理，只能通过实现接口的方式
						Object proxy = Proxy.newProxyInstance(listenerType.getClassLoader(), new Class<?>[]{listenerType}, handler);
						
						//client客户端
						setEventListenerMtd.invoke(view, proxy);
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 注入视图，视图注入到属性上
	 * @param activity
	 */
	private static void injectViews(Activity activity) {
		//通过Class获取类的属性列表
		Class<?> clazz = activity.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			//只获取带有ViewInject注解的属性
			ViewInject viewInject = field.getAnnotation(ViewInject.class);
			if(viewInject != null){
				//控件的id，也就是需要给属性注入的控件
				int viewId = viewInject.value();
				try {
					//通过findViewById获取到控件的实例
					//Method method = clazz.getMethod(METHOD_FIND_VIEW_BY_ID, int.class);
					//Object view = method.invoke(activity, viewId);
					View view = activity.findViewById(viewId);
					//允许访问私有的属性
					field.setAccessible(true);
					//把控件赋值给属性
					field.set(activity, view);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
	}



	/**
	 * 注入布局
	 * @param activity
	 */
	private static void injectContentView(Activity activity) {
		Log.d("jason", "注入布局"+activity);
		//通过对象获取到Mainctivity Class
		Class<?> clazz = activity.getClass();
		//获取Mainctivity Class上的ContentView注解
		ContentView contentView = clazz.getAnnotation(ContentView.class);
		//获取注解的值（布局id）
		int layoutId = contentView.value();
		try {
			//获取setContentView的方法
			Method method = clazz.getMethod(METHOD_SET_CONTENT_VIEW, int.class);
			//执行setContentView方法
			method.invoke(activity, layoutId);
			//activity.setContentView(layoutId);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
}
