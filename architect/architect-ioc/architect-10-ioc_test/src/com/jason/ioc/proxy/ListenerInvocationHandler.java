package com.jason.ioc.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

import android.app.Activity;

/**
 * 监听代理的句柄
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月23日
 * @version 1.0
 */
public class ListenerInvocationHandler implements InvocationHandler {

	private Map<String, Method> methodMap;
	private Activity activity;
	
	public ListenerInvocationHandler(Activity activity, Map<String, Method> methodMap) {
		this.activity = activity;
		this.methodMap = methodMap;
	}

	//listenerType 中声明的所有方法都是在invoke方法中执行
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String name = method.getName();
		Method mtd = methodMap.get(name);
		if(mtd != null){
			//当代理对象中OnClick方法传入时，我们执行MainActivity中mClick方法
			return mtd.invoke(activity, args);
		}
		//其他我们不需要拦截的方法，放行
		//OnClickListener有且只有一个方法，但是OnPageChangeListener有多个方法，我们有时只处理一个
		return method.invoke(proxy, args);
	}

}
